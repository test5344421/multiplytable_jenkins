package com.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class FileProcessing {
    private static final Logger logger = LoggerFactory.getLogger(FileProcessing.class);
    private static final String FILE_NAME = "config.properties";
    private static final String DIR_NAME = "conf/";

    public static String readProperty(String key) {
//        String line = "";
//        Properties properties = new Properties();
//        try {
//            line = readPropertyIntoJar(properties, key);
//        } catch (NullPointerException | IOException e) {
//            logger.error("Properties were not loaded {}", e.getMessage(), e);
//            throw new RuntimeException();
//        }
        Properties properties = new Properties();
        String line = "";
        try {
            line = readPropertyOutOfJar(properties, key);
        } catch (NullPointerException | IOException e) {
            logger.error("Properties were not loaded {}", e.getMessage(), e);
        }
        if (line == null) {
            try {
                line = readPropertyIntoJar(properties, key);
            } catch (NullPointerException | IOException e) {
                logger.error("Properties were not loaded {}", e.getMessage(), e);
                throw new RuntimeException();
            }
        }
        return line;
    }

    static String readPropertyIntoJar(Properties properties, String key) throws IOException {

        InputStream inputStream = App.class.getClassLoader().getResourceAsStream(FILE_NAME);
        if (inputStream != null) {

            loadingProperties(inputStream, properties, key);
        } else {
            logger.error("Properties into jar were not loaded ");
        }

        return properties.getProperty(key);
    }

    public static String readPropertyOutOfJar(Properties properties, String key) throws IOException {

        InputStream inputStream = new FileInputStream(DIR_NAME + FILE_NAME);
        loadingProperties(inputStream, properties, key);

        return properties.getProperty(key);
    }

    public static boolean loadingProperties(InputStream inputStream, Properties properties,String key) throws IOException {
        Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
        properties.load(reader);
        if (properties.getProperty(key) != null) {
            logger.debug("Properties were loaded");
            return true;
        }
        inputStream.close();
        return false;
    }
}
