package com.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class DataProcessing {

    private static final Logger logger = LoggerFactory.getLogger(DataProcessing.class);


    public double minimum() {
        return parseNumber(FileProcessing.readProperty("min"), readOutputFormat()).doubleValue();
    }

    public double maximum() {
        return parseNumber(FileProcessing.readProperty("max"), readOutputFormat()).doubleValue();
    }

    public double increment() {
        return parseNumber(FileProcessing.readProperty("increment"), readOutputFormat()).doubleValue();
    }

    public ArrayList<Double> addDigitsInArrayList(double min, double max, double increment) {
        ArrayList<Double> list = new ArrayList<>();
        for (double i = min; i <= max; i += increment) {
            list.add(i);
        }
        return list;
    }

    public ArrayList<Double> addDigitsInArrayListIfMinMoreThenMax(double min, double max, double increment) {
        ArrayList<Double> list = new ArrayList<>();
        for (double i = min; i >= max; i -= increment) {
            list.add(i);
        }
        return list;
    }

    public ArrayList<Double> getArrayList(double min, double max, double increment) {
        ArrayList<Double> list;
        if (min <= max) {
            list = addDigitsInArrayList(min, max, increment);
        } else {
            list = addDigitsInArrayListIfMinMoreThenMax(min, max, increment);
        }
        return list;
    }

    public String readOutputFormat() {
        String outputFormat = System.getProperty("type");
        if (outputFormat == null) {
            outputFormat = "int";
        }
        return outputFormat;
    }

    public Number parseNumber(String value, String outputFormat) {
        try {
            if (intType(outputFormat)) {
                return (Integer.parseInt(value));
            } else if (byteType(outputFormat)) {
                return (Byte.parseByte(value));
            } else if (doubleType(outputFormat)) {
                return (Double.parseDouble(value));
            } else if (longType(outputFormat)) {
                return (Long.parseLong(value));
            } else if (floatType(outputFormat)) {
                return (Float.parseFloat(value));
            } else {
                throw new NumberFormatException("Unsupported type");
            }
        } catch (NumberFormatException e) {
            logger.error("Inputted number {} is not {} ", value, outputFormat);
            throw new NumberFormatException("Unsupported type");
        }
    }

    public static boolean intType(String outputFormat) {
        return (outputFormat == null || outputFormat.equals("int"));
    }

    public static boolean doubleType(String outputFormat) {
        return outputFormat.equals("double");
    }

    public static boolean floatType(String outputFormat) {
        return outputFormat.equals("float");
    }

    public static boolean longType(String outputFormat) {
        return outputFormat.equals("long");
    }

    public static boolean byteType(String outputFormat) {
        return outputFormat.equals("byte");
    }

}
