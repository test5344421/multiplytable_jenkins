package com.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;

public class PrintingResults {
    private static final Logger logger = LoggerFactory.getLogger(PrintingResults.class);


    public String printResults(ArrayList<Double> multiplier, String outputFormat) {
        StringBuilder stringBuilder = new StringBuilder();
        for (double multiplierOne : multiplier) {
            for (double multiplierTwo : multiplier) {
                String res = Multiply.multiplyPrimitives(multiplierOne, multiplierTwo, outputFormat).toString();
                stringBuilder.append("\n").append(multiplierOne).append(" * ")
                        .append(multiplierTwo).append(" = ").append(res);
                logger.debug("{} * {} = {}", multiplierOne, multiplierTwo, res);
            }
        }
        return stringBuilder.toString();
    }

    public String tableAsString(double min, double max, double increment, String outputFormat) throws IOException {
        String s;
        if (increment <= 0) {
            throw new IOException("Increment must be positive");
        } else {
            ArrayList<Double> list = new DataProcessing().getArrayList(min, max, increment);
            s = printResults(list, outputFormat);
        }
        return s;
    }

    static double resultDouble(double result) {
        if (!Double.isInfinite(result)) {
            return (double) Math.round(result * 1000) / 1000;
        } else {
            throw new ArithmeticException("Double overflow resulting in INFINITY");
        }
    }

    static long resultLong(double result) {
        if (result <= Long.MAX_VALUE && result >= Long.MIN_VALUE) {
            return (long) result;
        } else {
            throw new ArithmeticException("Long overflow resulting in incorrect value");
        }
    }

    static float resultFloat(double result) {
        if (!Float.isInfinite((float) result)) {
            return Math.round(result * 1000) / 1000f;
        } else {
            throw new ArithmeticException("Float overflow resulting in INFINITY");
        }
    }

    static byte resultByte(double result) {
        if (result <= Byte.MAX_VALUE && result >= Byte.MIN_VALUE) {
            return (byte) result;
        } else {
            throw new ArithmeticException("byte overflow resulting in incorrect value");
        }
    }

    static int resultInt(double result) {
        if (result <= Integer.MAX_VALUE && result >= Integer.MIN_VALUE) {
            return (int) result;
        } else {
            throw new ArithmeticException("Integer overflow resulting in incorrect value");
        }
    }
}
