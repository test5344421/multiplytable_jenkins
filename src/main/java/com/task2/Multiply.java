package com.task2;

public class Multiply {

    public static Number multiplyPrimitives(double i, double j, String outputFormat) {
        double result = multiply(i, j);
        if (DataProcessing.intType(outputFormat)) {
            return PrintingResults.resultInt(result);
        } else if (DataProcessing.byteType(outputFormat)) {
            return PrintingResults.resultByte(result);
        } else if (DataProcessing.doubleType(outputFormat)) {
            return PrintingResults.resultDouble(result);
        } else if (DataProcessing.longType(outputFormat)) {
            return PrintingResults.resultLong(result);
        } else if (DataProcessing.floatType(outputFormat)) {
            return PrintingResults.resultFloat(result);
        } else {
            throw new ArithmeticException("Unknown data type");
        }
    }

    public static double multiply(double x, double y) {
        return x * y;
    }
}
