package com.task2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws IOException {
        logger.info("Program starts!");

        double a;
        for (int i= 64; i <= 1000000; i++) {
            a = 1000000.0 / i;
            if (a % 1 == 0) {

              //  String b = a + "";
                long c =(long) a;
                String d  = c + "";
//                for (int j = 0; j < b.length(); j++) {
                    if (!d.contains("0") && !(i+"").contains("0")) {
                        long n = c * i;
                        String m = n+"";
                        System.out.println(" a = " + d + " b = " + i);
                        System.out.println(c * i ) ;
//                    }
                }

            }
        }

        PrintingResults printingResults = new PrintingResults();
        DataProcessing dataProcessing = new DataProcessing();

        String outputFormat = dataProcessing.readOutputFormat();

        printingResults.tableAsString(dataProcessing.minimum(), dataProcessing.maximum(),
                dataProcessing.increment(), outputFormat);

        logger.info("Program was finished");
    }
}
